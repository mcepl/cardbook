if ("undefined" == typeof(ovl_collected)) {
	var ovl_collected = {
		
		addCardFromDisplayAndName: function (aDisplayName, aEmail) {
			if (!(aDisplayName != null && aDisplayName !== undefined && aDisplayName != "")) {
				if (!(aEmail != null && aEmail !== undefined && aEmail != "")) {
					return;
				}
			}
			if (!cardbookRepository.isEmailRegistered(aEmail)) {
				Components.utils.import("chrome://cardbook/content/cardbookRepository.js");
				var myNewCard = new cardbookCardParser();
				myNewCard.uid = new UUID() + "";
				myNewCard.dirPrefId = cardbookRepository.cardbookCollectedCardsId;
				myNewCard.fn = aDisplayName;
				if (myNewCard.fn == "") {
					myNewCard.fn = aEmail.substr(0, aEmail.indexOf("@")).replace("."," ").replace("_"," "); 
				}
				myNewCard.email = [ [ [aEmail], [] ,"", [] ] ];
				myNewCard.version = "3.0";
				cardbookRepository.addCardToRepository(myNewCard, cardbookUtils.getFileCacheNameFromCard(myNewCard, "CACHE"));
			}
		},
		
		addCardFromEmail: function (aEmails) {
			var listOfEmails = [];
			listOfEmails = cardbookUtils.getDisplayNameAndEmail(aEmails);
			for (var i = 0; i < listOfEmails.length; i++) {
				ovl_collected.addCardFromDisplayAndName(listOfEmails[i][0], listOfEmails[i][1]);
			}
		},
				
		collectToCardbook: function () {
			var prefs = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);
			var emailCollection = prefs.getBoolPref("extensions.cardbook.emailCollection");
			if (emailCollection) {
				var myFields = window.gMsgCompose.compFields;
				var listToCollect = ["replyTo", "to", "cc", "fcc", "bcc", "followupTo"];
				for (var i = 0; i < listToCollect.length; i++) {
					if (myFields[listToCollect[i]]) {
						if (myFields[listToCollect[i]] != null && myFields[listToCollect[i]] !== undefined && myFields[listToCollect[i]] != "") {
							ovl_collected.addCardFromEmail(myFields[listToCollect[i]]);
						}
					}
				}
			}
		},

		loadCollectedAccount: function () {
			var contactManager = Components.classes["@mozilla.org/abmanager;1"].getService(Components.interfaces.nsIAbManager);
			var contacts = contactManager.directories;
			while ( contacts.hasMoreElements() ) {
				var contact = contacts.getNext().QueryInterface(Components.interfaces.nsIAbDirectory);
				 if (contact.dirPrefId == "ldap_2.servers.history") {
					var abCardsEnumerator = contact.childCards;
					while (abCardsEnumerator.hasMoreElements()) {
						var abCard = abCardsEnumerator.getNext();
						abCard = abCard.QueryInterface(Components.interfaces.nsIAbCard);
						ovl_collected.addCardFromEmail(abCard.getProperty("DisplayName","") + " <" + abCard.getProperty("PrimaryEmail","") + ">");
					}
				 }
			}
		},
				
		addCollectedAccount: function() {
			var cacheDir = cardbookRepository.getLocalDirectory();
			cacheDir.append(cardbookRepository.cardbookCollectedCardsId);
	
			cardbookRepository.jsInclude(["chrome://cardbook/content/preferences/cardbookPreferences.js"]);
			let cardbookPrefService = new cardbookPreferenceService(cardbookRepository.cardbookCollectedCardsId);
			cardbookPrefService.setId(cardbookRepository.cardbookCollectedCardsId);
			cardbookPrefService.setName(cardbookRepository.cardbookCollectedCards);
			cardbookPrefService.setType("CACHE");
			cardbookPrefService.setUrl(cacheDir.path);
			cardbookPrefService.setUser("");
			cardbookPrefService.setColor("#A8C2E1");
			cardbookPrefService.setEnabled(true);
			cardbookPrefService.setExpanded(true);
			
			cardbookRepository.addAccountToRepository(cardbookRepository.cardbookCollectedCardsId, cardbookRepository.cardbookCollectedCards, "CACHE", true, true);
			cardbookMailPopularity.removeMailPopularity();
		}
		
	}
};

// SendMessage
(function() {
	// Keep a reference to the original function.
	var _original = SendMessage;
	
	// Override a function.
	SendMessage = function() {
		// Execute original function.
		var rv = _original.apply(null, arguments);
		
		// Execute some action afterwards.
		ovl_collected.collectToCardbook();

		// return the original result
		return rv;
	};

})();

// SendMessageLater
(function() {
	// Keep a reference to the original function.
	var _original = SendMessageLater;
	
	// Override a function.
	SendMessageLater = function() {
		// Execute original function.
		var rv = _original.apply(null, arguments);
		
		// Execute some action afterwards.
		ovl_collected.collectToCardbook();
		
		// return the original result
		return rv;
	};

})();
