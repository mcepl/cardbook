if ("undefined" == typeof(wdw_addressEdition)) {  
	var wdw_addressEdition = {
		
		load: function () {
			Components.utils.import("chrome://cardbook/content/cardbookRepository.js");
			document.getElementById('addressPostOfficeTextBox').value = cardbookUtils.undefinedToBlank(window.arguments[0].addressLine[0][0]);
			document.getElementById('addressExtendedAddrTextBox').value = cardbookUtils.undefinedToBlank(window.arguments[0].addressLine[0][1]);
			document.getElementById('addressStreetTextBox').value = cardbookUtils.undefinedToBlank(window.arguments[0].addressLine[0][2]);
			document.getElementById('addressLocalityTextBox').value = cardbookUtils.undefinedToBlank(window.arguments[0].addressLine[0][3]);
			document.getElementById('addressRegionTextBox').value = cardbookUtils.undefinedToBlank(window.arguments[0].addressLine[0][4]);
			document.getElementById('addressPostalCodeTextBox').value = cardbookUtils.undefinedToBlank(window.arguments[0].addressLine[0][5]);
			document.getElementById('addressCountryTextBox').value = cardbookUtils.undefinedToBlank(window.arguments[0].addressLine[0][6]);
			cardbookTypes.loadTypes("address", cardbookUtils.notNull(window.arguments[0].addressLine[3], window.arguments[0].addressLine[1]));
			document.getElementById('addressStreetTextBox').focus();
		},

		save: function () {
			if (cardbookTypes.validateTypes()) {
				window.arguments[0].addressLine[0][0] = document.getElementById('addressPostOfficeTextBox').value;
				window.arguments[0].addressLine[0][1] = document.getElementById('addressExtendedAddrTextBox').value;
				window.arguments[0].addressLine[0][2] = document.getElementById('addressStreetTextBox').value;
				window.arguments[0].addressLine[0][3] = document.getElementById('addressLocalityTextBox').value;
				window.arguments[0].addressLine[0][4] = document.getElementById('addressRegionTextBox').value;
				window.arguments[0].addressLine[0][5] = document.getElementById('addressPostalCodeTextBox').value;
				window.arguments[0].addressLine[0][6] = document.getElementById('addressCountryTextBox').value;
				var parsedTypes = cardbookTypes.getParsedTypes("address", window.arguments[0].addressLine[2]);
				window.arguments[0].addressLine[1] = parsedTypes.types;
				window.arguments[0].addressLine[2] = parsedTypes.pgName;
				window.arguments[0].addressLine[3] = parsedTypes.pgValue;
				window.arguments[0].action = "SAVE";
				close();
			}
		},

		cancel: function () {
			window.arguments[0].action = "CANCEL";
			close();
		}

	}; 

};