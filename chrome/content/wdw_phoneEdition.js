if ("undefined" == typeof(wdw_phoneEdition)) {  
	var wdw_phoneEdition = {
		
		load: function () {
			var strBundle = document.getElementById("cardbook-strings");
			var myWindowTitle = strBundle.getString("phoneEditionTitle");
			document.title = myWindowTitle;
			document.getElementById('phoneValueTextBox').value = window.arguments[0].phoneLine[0];
			cardbookTypes.loadTypes("phone", window.arguments[0].phoneLine[1]);
		},

		save: function () {
			window.arguments[0].phoneLine[0] = document.getElementById('phoneValueTextBox').value;
			window.arguments[0].phoneLine[1] = cardbookTypes.getTypes("phone");
			window.arguments[0].phoneLineAction="SAVE";
			close();
		},

		cancel: function () {
			window.arguments[0].phoneLineAction="CANCEL";
			close();
		}

	}; 

};