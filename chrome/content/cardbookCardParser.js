function cardbookCardParser(vCardData, vSiteUrl, vEtag, vDirPrefId) {
	this._init();
	
	if (vCardData != null && vCardData !== undefined && vCardData != "") {
		this._parseCard(vCardData, vSiteUrl, vEtag, vDirPrefId);
	}
}

cardbookCardParser.prototype = {
	jsInclude: function(files, target) {
		var loader = Components.classes["@mozilla.org/moz/jssubscript-loader;1"].getService(Components.interfaces.mozIJSSubScriptLoader);
		for (var i = 0; i < files.length; i++) {
			try {
				loader.loadSubScript(files[i], target);
			}
			catch(e) {
				dump("cardbookCardParser.jsInclude : failed to include '" + files[i] + "'\n" + e + "\n");
			}
		}
	},

    _init: function () {
		this.dirPrefId = "";
		this.cardurl = "";
		this.etag = "";
		this.updated = false;
		this.deleted = false;
		this.created = false;

		this.lastname = "";
		this.firstname = "";
		this.othername = "";
		this.prefixname = "";
		this.suffixname = "";
		this.fn = "";
		this.nickname = "";
		this.photo = "";
		this.bday = "";
		this.adr = [];
		this.label = [];
		this.tel = [];
		this.email = [];
		this.mailer = "";
		this.tz = "";
		this.geo = "";
		this.title = "";
		this.role = "";
		this.logo = "";
		this.agent = "";
		this.org = "";
		this.categories = [];
		this.note = "";
		this.prodid = "";
		this.sortstring = "";
		this.sound = "";
		this.uid = "";
		this.rev = "";
		this.url = [];
		this.version = "";
		this.class1 = "";
		this.key = "";
		this.impp = [];
		this.others = [];

		this.dispn = "";
		this.dispadr = "";
		this.disphomeadr = "";
		this.dispworkadr = "";
		this.displabel = "";
		this.disphomelabel = "";
		this.dispworklabel = "";
		this.disptel = "";
		this.disphometel = "";
		this.dispworktel = "";
		this.dispcelltel = "";
		this.dispemail = "";
		this.disphomeemail = "";
		this.dispworkemail = "";
		this.dispimpp = "";
		this.dispurl = "";

		this.pgname = "";
		this.pg = {};
	},

	addTypeToField: function (aField, aValueArray, aTypeArray) {
		if (this[aField].length === -1) {
			this[aField].push([aValueArray, aTypeArray, this.pgname, []]);
		} else {
			for (var i = 0; i < this[aField].length; i++) {
				if (this[aField][i][0].join() == aValueArray.join()) {
					this[aField][i][1].concat(aTypeArray);
					return;
				}
			}
			this[aField].push([aValueArray, aTypeArray, this.pgname, []]);
		}
	},

	addPgToField: function (aPgName, aPgValue) {
		var aField = this.pg[aPgName][1];
		if (this[aField] != null && this[aField] !== undefined && this[aField] != "") {
			for (var i = 0; i < this[aField].length; i++) {
				if (this[aField][i][2] == aPgName) {
					this[aField][i][3] = [aPgValue];
					return true;
				}
			}
		}
		return false;
	},
	
	findPropertyGroup: function (aKey) {
		if (aKey.indexOf(".") >= 0) {
			var aArray = [];
			aArray = aKey.split(".");
			this.pgname = aArray[0];
			if (aArray[1] !== "X-ABLABEL") {
				if (!(this.pg[this.pgname] != null && this.pg[this.pgname] !== undefined && this.pg[this.pgname] != "")) {
					this.pg[this.pgname] = [this.pgname, aArray[1].toLowerCase()];
				}
			}
			return aArray[1];
		} else {
			this.pgname = "";
			return aKey;
		}
	},

    _parseCard: function (vCardData, vSiteUrl, vEtag, vDirPrefId) {
		// var prompts = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
		// var errorTitle = "toto";
		// var errorMsg = vCardData;
		// prompts.alert(null, errorTitle, errorMsg);

		var tmpdispadr = [];
		var tmpdisphomeadr = [];
		var tmpdispworkadr = [];
		var tmpdisplabel = [];
		var tmpdisphomelabel = [];
		var tmpdispworklabel = [];
		var tmpdisptel = [];
		var tmpdisphometel = [];
		var tmpdispworktel = [];
		var tmpdispcelltel = [];
		var tmpdispemail = [];
		var tmpdisphomeemail = [];
		var tmpdispworkemail = [];
		var tmpdispimpp = [];
		var tmpdispurl = [];
		this.jsInclude(["chrome://cardbook/content/cardbookUtils.js"]);
		var re = /[\n\u0085\u2028\u2029]|\r\n?/;
		var vCardDataArray = vCardData.split(re);
		if (vCardDataArray.indexOf("VERSION:3.0") >= 0 || vCardDataArray.indexOf("VERSION:4.0") >= 0) {
			for (var vCardDataArrayIndex = 0; vCardDataArrayIndex < vCardDataArray.length-1;) {
				var localDelim1 = -1;
				var localDelim2 = -1;
				var vCardDataArrayHeaderKey = "";
				var vCardDataArrayHeaderOption = "";
				var vCardDataArrayTrailer = "";
				var vCardDataArrayTrailerArray = [];
				var vCardDataArrayHeaderOptionArray = [];
				var localDelimArray = null;
				localDelim1 = vCardDataArray[vCardDataArrayIndex].indexOf(":",0);
				vCardDataArrayHeaderKey = "";
				vCardDataArrayHeaderOption = "";
				vCardDataArrayTrailer = "";
				
				// Splitting data
				if (localDelim1 >= 0) {
					vCardDataArrayHeader = vCardDataArray[vCardDataArrayIndex].substr(0,localDelim1);
					vCardDataArrayTrailer = vCardDataArray[vCardDataArrayIndex].substr(localDelim1+1,vCardDataArray[vCardDataArrayIndex].length);
					localDelim2 = vCardDataArrayHeader.indexOf(";",0);
					if (localDelim2 >= 0) {
						vCardDataArrayHeaderKey = vCardDataArrayHeader.substr(0,localDelim2).toUpperCase();
						vCardDataArrayHeaderOption = vCardDataArrayHeader.substr(localDelim2+1,vCardDataArrayHeader.length);
					} else {
						vCardDataArrayHeaderKey = vCardDataArrayHeader.toUpperCase();
						vCardDataArrayHeaderOption = "";
					}
				}

				// For multilines data
				if ( vCardDataArrayIndex < vCardDataArray.length-1) {
					vCardDataArrayIndex++;
					var vLoop = true;
					while (vLoop) {
						if (vCardDataArray[vCardDataArrayIndex].substr(0,1) == " " ) {
							vCardDataArrayTrailer = vCardDataArrayTrailer + vCardDataArray[vCardDataArrayIndex].substr(1);
							if (vCardDataArrayIndex < vCardDataArray.length-1) {
								vCardDataArrayIndex++;
							} else {
								vLoop = false;
							}
						} else {
							vLoop = false;
						}
					}
				}
				
				switch (this.findPropertyGroup(vCardDataArrayHeaderKey)) {
					case "BEGIN":
						break;
					case "END":
						break;
					case "N":
						vCardDataArrayTrailerArray = [];
						vCardDataArrayTrailerArray = cardbookUtils.escapeString(vCardDataArrayTrailer).split(";");
						if (vCardDataArrayTrailerArray[0]) {
							this.lastname = cardbookUtils.unescapeString(vCardDataArrayTrailerArray[0]);
						} else {
							this.lastname = "";
						}
						if (vCardDataArrayTrailerArray[1]) {
							this.firstname = cardbookUtils.unescapeString(vCardDataArrayTrailerArray[1]);
						} else {
							this.firstname = "";
						}
						if (vCardDataArrayTrailerArray[2]) {
							this.othername = cardbookUtils.unescapeString(vCardDataArrayTrailerArray[2]);
						} else {
							this.othername = "";
						}
						if (vCardDataArrayTrailerArray[3]) {
							this.prefixname = cardbookUtils.unescapeString(vCardDataArrayTrailerArray[3]);
						} else {
							this.prefixname = "";
						}
						if (vCardDataArrayTrailerArray[4]) {
							this.suffixname = cardbookUtils.unescapeString(vCardDataArrayTrailerArray[4]);
						} else {
							this.suffixname = "";
						}
						this.dispn = cardbookUtils.unescapeString(vCardDataArrayTrailer);
						break;
					case "FN":
						this.fn = cardbookUtils.unescapeString(vCardDataArrayTrailer);
						break;
					case "NICKNAME":
						this.nickname = cardbookUtils.unescapeString(vCardDataArrayTrailer);
						break;
					case "PHOTO":
						this.photo = vCardDataArrayHeaderOption + ":" + vCardDataArrayTrailer;
						break;
					case "BDAY":
						this.bday = vCardDataArrayTrailer;
						break;
					case "ADR":
						vCardDataArrayTrailerArray = [];
						vCardDataArrayHeaderOptionArray = [];
						vCardDataArrayHeaderOptionArray = vCardDataArrayHeaderOption.replace(/type=/gi,"").replace(",",";").split(";");
						vCardDataArrayTrailerArray = cardbookUtils.escapeString(vCardDataArrayTrailer).split(";");
						var unescaped = cardbookUtils.unescapeArray(vCardDataArrayTrailerArray);
						for (let adrOptionsIndex = 0; adrOptionsIndex < vCardDataArrayHeaderOptionArray.length; adrOptionsIndex++) {
							switch (vCardDataArrayHeaderOptionArray[adrOptionsIndex].toUpperCase()) {
								case "HOME":
									tmpdisphomeadr.push(unescaped);
									break;
								case "WORK":
									tmpdispworkadr.push(unescaped);
									break;
							}
						}
						tmpdispadr.push(unescaped);
						this.addTypeToField("adr", cardbookUtils.formatArray(vCardDataArrayTrailerArray), vCardDataArrayHeaderOptionArray);
						break;
					case "LABEL":
						vCardDataArrayTrailerArray = [];
						vCardDataArrayHeaderOptionArray = [];
						vCardDataArrayHeaderOptionArray = vCardDataArrayHeaderOption.replace(/type=/gi,"").replace(",",";").split(";");
						vCardDataArrayTrailerArray = cardbookUtils.escapeString(vCardDataArrayTrailer).split(";");
						var unescaped = cardbookUtils.unescapeArray(vCardDataArrayTrailerArray);
						for (let labelOptionsIndex = 0; labelOptionsIndex < vCardDataArrayHeaderOptionArray.length; labelOptionsIndex++) {
							switch (vCardDataArrayHeaderOptionArray[labelOptionsIndex].toUpperCase()) {
								case "HOME":
									tmpdisphomelabel.push(unescaped);
									break;
								case "WORK":
									tmpdispworklabel.push(unescaped);
									break;
							}
						}
						tmpdisplabel.push(unescaped);
						this.addTypeToField("label", cardbookUtils.unescapeArray(vCardDataArrayTrailerArray), vCardDataArrayHeaderOptionArray);
						break;
					case "TEL":
						vCardDataArrayTrailerArray = [];
						vCardDataArrayHeaderOptionArray = [];
						vCardDataArrayHeaderOptionArray = vCardDataArrayHeaderOption.replace(/type=/gi,"").replace(",",";").split(";");
						vCardDataArrayTrailerArray = cardbookUtils.escapeString(vCardDataArrayTrailer).split(";");
						var unescaped = cardbookUtils.unescapeArray(vCardDataArrayTrailerArray);
						for (let telOptionsIndex = 0; telOptionsIndex < vCardDataArrayHeaderOptionArray.length; telOptionsIndex++) {
							switch (vCardDataArrayHeaderOptionArray[telOptionsIndex].toUpperCase()) {
								case "HOME":
									tmpdisphometel.push(unescaped);
									break;
								case "WORK":
									tmpdispworktel.push(unescaped);
									break;
								case "CELL":
									tmpdispcelltel.push(unescaped);
									break;
							}
						}
						tmpdisptel.push(unescaped);
						this.addTypeToField("tel", cardbookUtils.unescapeArray(vCardDataArrayTrailerArray), vCardDataArrayHeaderOptionArray);
						break;
					case "EMAIL":
						vCardDataArrayTrailerArray = [];
						vCardDataArrayHeaderOptionArray = [];
						vCardDataArrayHeaderOptionArray = vCardDataArrayHeaderOption.replace(/type=/gi,"").replace(",",";").split(";");
						vCardDataArrayTrailerArray = cardbookUtils.escapeString(vCardDataArrayTrailer).split(";");
						var unescaped = cardbookUtils.unescapeArray(vCardDataArrayTrailerArray);
						for (let emailOptionsIndex = 0; emailOptionsIndex < vCardDataArrayHeaderOptionArray.length; emailOptionsIndex++) {
							switch (vCardDataArrayHeaderOptionArray[emailOptionsIndex].toUpperCase()) {
								case "HOME":
									tmpdisphomeemail.push(unescaped);
									break;
								case "WORK":
									tmpdispworkemail.push(unescaped);
									break;
							}
						}
						tmpdispemail.push(unescaped);
						this.addTypeToField("email", cardbookUtils.unescapeArray(vCardDataArrayTrailerArray), vCardDataArrayHeaderOptionArray);
						break;
					case "MAILER":
						this.mailer = vCardDataArrayTrailer;
						break;
					case "TZ":
						this.tz = vCardDataArrayTrailer;
						break;
					case "GEO":
						this.geo = vCardDataArrayTrailer;
						break;
					case "TITLE":
						this.title = cardbookUtils.unescapeString(vCardDataArrayTrailer);
						break;
					case "ROLE":
						this.role = cardbookUtils.unescapeString(vCardDataArrayTrailer);
						break;
					case "LOGO":
						this.logo = vCardDataArrayHeaderOption + ":" + vCardDataArrayTrailer;
						break;
					case "AGENT":
						this.agent = vCardDataArrayHeaderOption + ":" + vCardDataArrayTrailer;
						break;
					case "ORG":
						this.org = cardbookUtils.unescapeString(vCardDataArrayTrailer);
						break;
					case "CATEGORIES":
						this.categories = cardbookUtils.unescapeArray(cardbookUtils.escapeString(vCardDataArrayTrailer).split(","));
						this.categories = cardbookRepository.arrayUnique(this.categories);
						break;
					case "NOTE":
						this.note = cardbookUtils.formatNote(vCardDataArrayTrailer);
						break;
					case "PRODID":
						this.prodid = vCardDataArrayTrailer;
						break;
					case "SORT-STRING":
						this.sortstring = vCardDataArrayTrailer;
						break;
					case "SOUND":
						this.sound = vCardDataArrayHeaderOption + ":" + vCardDataArrayTrailer;
						break;
					case "UID":
						this.uid = vCardDataArrayTrailer;
						break;
					case "URL":
						vCardDataArrayTrailerArray = [];
						vCardDataArrayHeaderOptionArray = [];
						vCardDataArrayHeaderOptionArray = vCardDataArrayHeaderOption.replace(/type=/gi,"").replace(",",";").split(";");
						vCardDataArrayTrailerArray = cardbookUtils.escapeString(vCardDataArrayTrailer).split(";");
						this.addTypeToField("url", cardbookUtils.unescapeArray(vCardDataArrayTrailerArray), vCardDataArrayHeaderOptionArray);
						break;
					case "VERSION":
						this.version = vCardDataArrayTrailer;
						break;
					case "CLASS":
						this.class1 = vCardDataArrayTrailer;
						break;
					case "KEY":
						this.key = vCardDataArrayHeaderOption + ":" + vCardDataArrayTrailer;
						break;
					case "REV":
						this.rev = vCardDataArrayTrailer;
						break;
					// outside ftp://ftp.rfc-editor.org/in-notes/rfc2426.txt
					case "IMPP":
						vCardDataArrayTrailerArray = cardbookUtils.escapeString(vCardDataArrayTrailer).split(";");
						this.addTypeToField("impp", cardbookUtils.unescapeArray(vCardDataArrayTrailerArray), vCardDataArrayHeaderOption.replace(/x-service-type=/gi,"").replace(/type=/gi,"").split(";"));
						break;
					case "X-THUNDERBIRD-ETAG":
						this.etag = vCardDataArrayTrailer;
						this.others.push(vCardDataArrayHeaderKey + ":" + vCardDataArrayTrailer);
						break;
					case "X-THUNDERBIRD-MODIFICATION":
						switch (vCardDataArrayTrailer) {
							case "UPDATED":
								this.updated = true;
								break;
							case "CREATED":
								this.created = true;
								break;
							case "DELETED":
								this.deleted = true;
								break;
							default:
								break;
						}
						this.others.push(vCardDataArrayHeaderKey + ":" + vCardDataArrayTrailer);
						break;
					case "X-ABLABEL":
						if (this.pg[this.pgname] != null && this.pg[this.pgname] !== undefined && this.pg[this.pgname] != "") {
							if (!(this.addPgToField(this.pgname, vCardDataArrayTrailer))) {
								this.others.push(vCardDataArrayHeaderKey + ":" + vCardDataArrayTrailer);
							}
						} else {
							this.others.push(vCardDataArrayHeaderKey + ":" + vCardDataArrayTrailer);
						}
						break;
					default:
						if (vCardDataArrayHeaderKey != null && vCardDataArrayHeaderKey !== undefined && vCardDataArrayHeaderKey != "") {
							this.others.push(vCardDataArrayHeaderKey + ":" + vCardDataArrayTrailer);
						}
				}
			}

			this.disphomeadr = cardbookUtils.parseArray(tmpdisphomeadr);
			this.dispworkadr = cardbookUtils.parseArray(tmpdispworkadr);
			this.disphomelabel = cardbookUtils.parseArray(tmpdisphomelabel);
			this.dispworklabel = cardbookUtils.parseArray(tmpdispworklabel);
			this.disphometel = cardbookUtils.parseArray(tmpdisphometel);
			this.dispworktel = cardbookUtils.parseArray(tmpdispworktel);
			this.dispcelltel = cardbookUtils.parseArray(tmpdispcelltel);
			this.disphomeemail = cardbookUtils.parseArray(tmpdisphomeemail);
			this.dispworkemail = cardbookUtils.parseArray(tmpdispworkemail);
			this.dispadr = cardbookUtils.parseArray(tmpdispadr);
			this.displabel = cardbookUtils.parseArray(tmpdisplabel);
			this.disptel = cardbookUtils.parseArray(tmpdisptel);
			this.dispemail = cardbookUtils.parseArray(tmpdispemail);
			this.dispurl = cardbookUtils.parseArrayByType(this.url)
			this.dispimpp = cardbookUtils.parseArrayByType(this.impp)
			
			this.dirPrefId = vDirPrefId;
			if (vSiteUrl != null && vSiteUrl !== undefined && vSiteUrl != "") {
				this.cardurl = vSiteUrl;
			}
			
			if (this.uid == "") {
				cardbookUtils.jsInclude(["chrome://cardbook/content/uuid.js"]);
				this.uid = new UUID() + "";
			}
			
			if (this.fn == "") {
				this.fn = cardbookUtils.getDisplayedName(this.fn, this.fn, this.dispn.split(";"), this.dispn.split(";"), this.org, this.org);
			}
			
			cardbookUtils.addEtag(this, vEtag);
		} else {
			var Application = Components.classes["@mozilla.org/steel/application;1"].getService(Components.interfaces.steelIApplication);
			Application.console.log("cardbook error with card : " + vCardData);
			function UserException(message) {
				this.message = message;
				this.name = "UserException";
			}
			throw new UserException("Only 3.0 or 4.0 version supported");
		}
    }
};