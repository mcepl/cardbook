if ("undefined" == typeof(wdw_labelEdition)) {  
	var wdw_labelEdition = {
		
		load: function () {
			document.getElementById('labelValueTextBox').value = cardbookUtils.undefinedToBlank(window.arguments[0].labelLine[0][0]);
			document.getElementById('labelValueTextBox').focus();
			cardbookTypes.loadTypes("label1", cardbookUtils.notNull(window.arguments[0].labelLine[3], window.arguments[0].labelLine[1]));
		},

		save: function () {
			if (cardbookTypes.validateTypes()) {
				window.arguments[0].labelLine[0][0] = document.getElementById('labelValueTextBox').value;
				var parsedTypes = cardbookTypes.getParsedTypes("label1", window.arguments[0].labelLine[2]);
				window.arguments[0].labelLine[1] = parsedTypes.types;
				window.arguments[0].labelLine[2] = parsedTypes.pgName;
				window.arguments[0].labelLine[3] = parsedTypes.pgValue;
				window.arguments[0].action = "SAVE";
				close();
			}
		},

		cancel: function () {
			window.arguments[0].action = "CANCEL";
			close();
		}

	}; 

};