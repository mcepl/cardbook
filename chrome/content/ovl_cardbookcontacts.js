function hideOldAddressbook() {
	document.getElementById("addToAddressBookItem").setAttribute("hidden", true);
	document.getElementById("editContactItem").setAttribute("hidden", true);
	document.getElementById("viewContactItem").setAttribute("hidden", true);
}

function hideOrShowNewAddressbook(aValue) {
	if (aValue) {
		document.getElementById("addToCardbookMenu").setAttribute("hidden", true);
	} else {
		document.getElementById("addToCardbookMenu").removeAttribute("hidden");
	}
}

function onClickEmailStar(event, emailAddressNode) {
	return;
}

// setupEmailAddressPopup
(function() {
	// Keep a reference to the original function.
	var _original = setupEmailAddressPopup;
	
	// Override a function.
	setupEmailAddressPopup = function() {
		// Execute original function.
		var rv = _original.apply(null, arguments);
		
		// Execute some action afterwards.
		hideOldAddressbook();
		
		// return the original result
		return rv;
	};

})();

// UpdateEmailNodeDetails
(function() {
	// Keep a reference to the original function.
	var _original = UpdateEmailNodeDetails;
	
	// Override a function.
	UpdateEmailNodeDetails = function() {
		// Execute original function.
		var rv = _original.apply(null, arguments);
		
		// Execute some action afterwards.
		hideOldAddressbook();
		Components.utils.import("chrome://cardbook/content/cardbookRepository.js");
		var cardFound = cardbookRepository.isEmailRegistered(arguments[0]);
		arguments[1].setAttribute("hascard", cardFound.toString());
		hideOrShowNewAddressbook(cardFound);
		
		// return the original result
		return rv;
	};

})();
