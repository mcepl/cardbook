pref("extensions.cardbook.autocompletion", true);
pref("extensions.cardbook.emailCollection", true);
pref("extensions.cardbook.requestsTimeout", "120");
pref("extensions.cardbook.statusInformationLineNumber", "250");
pref("extensions.cardbook.debugMode", false);

pref("extensions.cardbook.buttonName", true);
pref("extensions.cardbook.buttonOrg", true);
pref("extensions.cardbook.buttonCategories", true);
pref("extensions.cardbook.buttonAddress", true);
pref("extensions.cardbook.buttonPhone", true);
pref("extensions.cardbook.buttonEmail", true);
pref("extensions.cardbook.buttonImpp", true);
pref("extensions.cardbook.buttonUrl", false);
pref("extensions.cardbook.buttonNote", true);
pref("extensions.cardbook.buttonBirthday", true);
pref("extensions.cardbook.preferEmailEdition", false);

pref("extensions.cardbook.defaultLook", true);
pref("extensions.cardbook.panesView", "classical");
pref("extensions.cardbook.preferDisk", true);
pref("extensions.cardbook.autoSync", true);
pref("extensions.cardbook.autoSyncInterval", "30");

pref("extensions.cardbook.preferEmailPref", false);
